using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class switchenvironment : MonoBehaviour
{
    public GameObject cny;
    public GameObject mamak;
    public GameObject divali;
    public GameObject hariraya;

    //infographic
    public GameObject cny_info;
    public GameObject mamak_info;
    public GameObject divali_info;
    public GameObject hariraya_info;
    public GameObject blur_bg;
    public GameObject buttonstochange;
    

    public GameObject mamak_interaction;
    public GameObject cnyinteraction;
    public GameObject depavali_interaction;
    public GameObject hari_raya_interaction;
  public GameObject memories_mamak;
    public GameObject cny_memories;
    public GameObject diwali_memories;
    public GameObject hariraya_memories;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       
    }
    public void Mamakbtn(){
        mamak.SetActive(true);
        cny.SetActive(false);
        divali.SetActive(false);
         hariraya.SetActive(false);
         mamak_info.SetActive(true);
         blur_bg.SetActive(true);
         buttonstochange.SetActive(false);
         mamak_interaction.SetActive(true);
         cnyinteraction.SetActive(false);
         depavali_interaction.SetActive(false);
         hari_raya_interaction.SetActive(false);
    }
    public void cnybtn(){
        cny.SetActive(true);
        divali.SetActive(false);
         hariraya.SetActive(false);
         mamak.SetActive(false);
         cny_info.SetActive(true);
         blur_bg.SetActive(true);
         buttonstochange.SetActive(false);
         cnyinteraction.SetActive(true);
          hari_raya_interaction.SetActive(false);
         depavali_interaction.SetActive(false);
           mamak_interaction.SetActive(false);
    }
    public void divalibtn(){
         divali.SetActive(true);
        cny.SetActive(false);
         hariraya.SetActive(false);
         mamak.SetActive(false);
         divali_info.SetActive(true);
         blur_bg.SetActive(true);
         buttonstochange.SetActive(false);
         depavali_interaction.SetActive(true);
         hari_raya_interaction.SetActive(false);
          cnyinteraction.SetActive(false);
           mamak_interaction.SetActive(false);
        
    }
    public void harirayabtn(){
        hariraya.SetActive(true);
        mamak.SetActive(false);
     cny.SetActive(false);
      divali.SetActive(false);
       hariraya_info.SetActive(true);
       blur_bg.SetActive(true);
       buttonstochange.SetActive(false);
       hari_raya_interaction.SetActive(true);
             mamak_interaction.SetActive(false);
             cnyinteraction.SetActive(false);
             depavali_interaction.SetActive(false);

    } 
    public void cross_info(){
        hariraya_info.SetActive(false);
        divali_info.SetActive(false);
         cny_info.SetActive(false);
          mamak_info.SetActive(false);
          blur_bg.SetActive(false);
          buttonstochange.SetActive(true);
    }
    public void memorybtn_mamak(){
        memories_mamak.SetActive(true);
    }
    public void cross_memorybtn_mamak(){
        memories_mamak.SetActive(false);
    }
    public void cny_memory_btn(){
        cny_memories.SetActive(true);
    }
    public void cny_memory_cross(){
        cny_memories.SetActive(false);
    }
    public void depavalimemories_btn(){
        diwali_memories.SetActive(true);
    }
    public void depavalimemories_cross(){
        diwali_memories.SetActive(false);
    }
    public void hariraya_memories_btn(){
        hariraya_memories.SetActive(true);
    }
    public void hariraya_memories_cross(){
        hariraya_memories.SetActive(false);
    }
}
