    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pagemanager : MonoBehaviour
{
    [SerializeField]
        private GameObject[] characters;
    public GameObject sign_in;

    public GameObject mainmenu;
    public GameObject homepagebtn;
    public GameObject signinpg;
    public GameObject fifthpage;

    public void ChangeCharacters(int index)
    {
        for (int i = 0; i < characters.Length; i++)
        {
            characters[i].SetActive(false);
        }
            characters[index].SetActive(true);
    }
    public void signinbtn(){
        sign_in.SetActive(true);
        mainmenu.SetActive(false);
    
    }
    public void homepage_setactive(){
        homepagebtn.SetActive(true);
        signinpg.SetActive(false);
        fifthpage.SetActive(false);
    }

 
}