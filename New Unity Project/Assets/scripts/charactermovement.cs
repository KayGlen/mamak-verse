using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class charactermovement : MonoBehaviour
{
    private Rigidbody2D rb;
    private bool moveleft, moveright, moveup , movedown;
    private float movespeed = 15f;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
  
        moveleft = false;
        moveright = false;
        moveup = false;
        movedown = false;
    }


    void Update()
    {

    }
    public void moverightbtn(){
        moveright = true;
        rb.MovePosition(transform.position + Vector3.right * movespeed * Time.deltaTime);
    }
    public void moveleftbtn(){
        moveleft = true;
        rb.MovePosition(transform.position + Vector3.left * movespeed * Time.deltaTime);
    }
    public void moveeupbtn(){
        moveleft = true;
        rb.MovePosition(transform.position + Vector3.up * movespeed * Time.deltaTime);
    }
    public void movedownbtn(){
        moveleft = true;
        rb.MovePosition(transform.position + Vector3.down * movespeed * Time.deltaTime);
    }
    public void let_go_of_button(){
        moveleft = false;
        moveright = false;       
         moveup = false;
        movedown = false;
    }
    

  
}
