using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mamakpoints : MonoBehaviour
{
    public GameObject mamakpoint01;
    public GameObject mamakpoint02;
    public GameObject voucherpage;
    public GameObject achievement;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void mamakpoint_pagefront(){
        mamakpoint01.SetActive(false);
        mamakpoint02.SetActive(true);
    }
    public void mamakpoint_pageback(){
        mamakpoint01.SetActive(true);
        mamakpoint02.SetActive(false);
        voucherpage.SetActive(false);
        achievement.SetActive(false);
    }
    public void vocherbtn(){
    voucherpage.SetActive(true);
     mamakpoint02.SetActive(false);
    }public void backvocherbtn(){
    voucherpage.SetActive(false);
     mamakpoint02.SetActive(true);
    }
    public void achieevementbtn(){
        achievement.SetActive(true);
        mamakpoint02.SetActive(false);
    }
     public void vouchertbtnback(){
        achievement.SetActive(false);
        mamakpoint02.SetActive(true);
    }
    public void achievementbtnback2(){
        achievement.SetActive(false);
        mamakpoint02.SetActive(true);
    }
}
