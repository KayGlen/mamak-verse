using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class drag : MonoBehaviour, IBeginDragHandler, IDragHandler 
{
    public Vector3 offset;

    #region IBeginDragHandler implementation  
    public void OnBeginDrag (PointerEventData eventData){
        // throw new System.NotImplementedException ();
        offset = transform.position - (Vector3)eventData.position;
    }
    #endregion

    #region IDragHandler implementation

    public void OnDrag (PointerEventData eventData){
        // throw new System.NotImplementedException ();
        // offset = transform.position + (Vector3)eventData.position;
        transform.position = (Vector3) eventData.position + offset;
    }
    // Start is called before the first frame update
    

    // Update is called once per frame
    
}
#endregion