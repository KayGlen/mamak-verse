using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fooddeals : MonoBehaviour
{
    public GameObject foodealspage01;
    public GameObject foodealspage02;
    public GameObject chinesepage;
    public GameObject loadingpage;
    public Animator sliderplay;
    public GameObject homepage;
    public GameObject fooddealspageeee;
  
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void fooddealbtn_foward(){
       
       
        foodealspage01.SetActive(false);
        foodealspage02.SetActive(true);
        
    }
    public void chinesecuisinefunction(){
        sliderplay.Play("slider");
          loadingpage.SetActive(true);
        StartCoroutine(waitToShowFBScene3());
        IEnumerator waitToShowFBScene3(){
            yield return new WaitForSeconds (2);
            loadingpage.SetActive(false);
        chinesepage.SetActive(true);
        foodealspage02.SetActive(false);
        }
    }
      public void goback_chinesecuisinefunction(){
        chinesepage.SetActive(false);
        foodealspage02.SetActive(true);
    }
    public void gobacktohomepage(){
        homepage.SetActive(true);
        fooddealspageeee.SetActive(false);
    }
}
