using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movementscriptunity : MonoBehaviour
{
    private Rigidbody2D rb;
    private bool moveLeft;
    private bool moveRight;
    private float Horizontalmove;
    public float speed;

    //vertical move
    private float movespeed;

    //Animation
    public Animator character_walk;
    public Animator character_idle;
    public Animator character_back_idle;
     public Animator character_back_walking;
     public Animator right_walking;
     public Animator right_idle;
     public Animator left_idle;
     
     public Animator left_walking;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        moveLeft = false;
        moveRight = false;
        movespeed = 2f;
        speed = 2f;
    }

    // Update is called once per frame
    void Update()
    {
        MovePlayer();
    }
    public void PointerDownLeft(){
        moveLeft = true;
        left_walking.Play("left_walking");
    }
    public void PointerUpLeft(){
        moveLeft = false;
        left_idle.Play("idle_left");
    }
      public void PointerDownRight(){
        moveRight = true;
        right_walking.Play("right_walking");
    }
    public void PointerUpRight(){
        moveRight = false;
        right_walking.Play("Idle_right");
    }
    private void MovePlayer(){
        if(moveLeft){
            Horizontalmove = -speed;
        }
        else if(moveRight){
             Horizontalmove = speed;
        }
        else{
            Horizontalmove = 0;
        }
    }

    public void Moveup(){
        rb.velocity = Vector2.up * movespeed;
        character_back_walking.Play("Back_walking");
        
        
    }
    public void Pointerup_Moveup(){
         character_back_idle.Play("back_idle");
           rb.velocity = Vector2.zero;
    }
    public void Movedown(){
        rb.velocity = Vector2.up * -movespeed;
       character_walk.Play("walk");
       
         
    }
      public void StopMoving(){
        rb.velocity = Vector2.zero;
        character_idle.Play("Idle");
    }
  private void FixedUpdate() {
        rb.velocity = new Vector2(Horizontalmove, rb.velocity.y);
    }
}
